'''
All of the "building block" classes
'''


'''
Parent class of function blocks, that transmit data to each other
It's kind of like a linked list, but I really want to make it all abstract and stuff, so here it is.
'''

CODEESCAPESEQUENCE='\\\\'

class FunctionBlock:
    
    previous_block = None
    next_block = None
    data = ""

    def __init__(self, pb = None, nb = None, data = None):
        self.previous_block = pb
        self.next_block = nb
        self.data = data
        #Setting previous/next nodes on next/previous nodes of this instance, if this one has any
        if not pb is None:
            pb.set_next_block(self)
        if not nb is None:
            nb.set_previous_block(self)
        

    def send_data(self):
        #TODO: Print actual object names instead of their classes
        print(self.__class__.__name__, 'sent', self.data, 'to', self.next_block.__class__.__name__)
        #TODO: Implement channel errors
        self.next_block.recieve_data(self.data)
    
    def recieve_data(self, data):
        self.data = data        
        print(self.__class__.__name__, 'recieved', self.data, 'from', self.previous_block.__class__.__name__)

    def set_next_block(self, nb):
        self.next_block = nb

    def set_previous_block(self, pb):
        self.previous_block = pb


class SignalSource(FunctionBlock):
    
    def recieve_data(self, data):
        print("ERROR: Sending data to source")


class SignalCoder(FunctionBlock):
    
    coding_table = {}

    def __init__(self, pb = None, nb = None, data = None, table = None):
        super(SignalCoder, self).__init__(pb, nb, data)
        self.coding_table = table

    def code_data(self, coding_table = None):
        table = self.coding_table
        if not coding_table is None:
            table = coding_table
        #Replacing the old data with the coded data, with each symbol-code separated by a specified code escape sequence
        self.data = CODEESCAPESEQUENCE.join([table[ch] for ch in self.data])

    def send_data(self):
        #Before we send the data, we gotta code it!
        self.code_data()
        super(SignalCoder, self).send_data()


'''
Inherits from coder, because both do the same thing, but in reversal
'''
class SignalDecoder(SignalCoder):
    def __init__(self, pb = None, nb = None, data = None, table = None):
        if not table is None:
            #Reversing the coding table, so that decoding is easy
            table = {value: key for key, value in table.items()}
        super(SignalDecoder, self).__init__(pb, nb, data, table)
    '''
    Some redundant sugar so it looks nice
    '''
    def decode_data(self, coding_table = None):
        self.code_data(coding_table)

    def code_data(self, coding_table = None):
        table = self.coding_table
        if not coding_table is None:
            table = coding_table
        #Here we suggest that the data is transmitted with spaces inbetween each code
        self.data = ''.join([table[code] for code in self.data.split(CODEESCAPESEQUENCE)])
        


class SignalReciever(FunctionBlock):

    def send_data(self, data):
        print("ERROR: Sending data from reciever")
