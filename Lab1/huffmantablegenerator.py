'''
Coding table generator for Huffman algorithm
'''

DEBUG = False

def generate_table(source_string):

    coding_table = {}
    probability_dict = {}

    for character in source_string:
        if character not in probability_dict:
            probability_dict[character] = 1
            #Adding our character to the final coding table
            coding_table[character] = ""
        else:
            probability_dict[character] += 1
    #Counting probabilities of each character
    for ch in probability_dict:
        probability_dict[ch] = probability_dict[ch] / float(len(source_string))
    if DEBUG:
        print(probability_dict)
    #Commence algorithm!
    while len(probability_dict) > 1:    
        #Step1: Sort the dictionary by probabilites, and get the two least used values
        least_used_values = sorted(probability_dict.keys(), key=probability_dict.get)[:2]
        if DEBUG:
            print(least_used_values)
        sum_prob = 0
        #Step2: Add 0 or 1 to the left of the coding of each letter in these strings
        for index, word in enumerate(least_used_values):
            for ch in word:
                coding_table[ch] = ''.join([str(index), coding_table[ch]])
            #Removing the old word from the dictionary and adding its probability to the new one
            sum_prob += probability_dict.pop(word)
        if DEBUG:
            print(coding_table)
        #Step3: Create a new word, consisting of used 2, and put it into the dictionary
        probability_dict[''.join(least_used_values)] = sum_prob
        #Step4: Rince and repeat, until we have only 1 word in the dictionary

    return coding_table
                

#because ~ doesn't work and python has signed ints :/
def inverse(a):
    mask = 2**(len(str(bin(a))) - 2) - 1 #substracting 2 from length cause of 0b prefix
    return ~a & mask

if __name__ == "__main__":
    DEBUG = TRUE
    generate_table("aaaaaabbbbbccddfc")
