import sys, funcblocks as fb, huffmantablegenerator as tablegen

HELPSTRING = '''Signal... something module, which uses Huffman algorithm for coding and decoding data\nUsage: either pass the string as an argument, or pass nothing and input it yourself'''

if len(sys.argv) == 1:
    input_string = input("Input the string:\n")
    if input_string.replace(' ','') == '':
        input_string = "Allan please add test string"
elif sys.argv[1] in ['-h', '--h', '/?']:
    print(HELPSTRING)
    sys.exit(0)
else:
    #TODO: more than one input, maybe?
    input_string = ''.join(sys.argv[1:])

coding_table = tablegen.generate_table(input_string)
print('Huffman coding table:\n', coding_table)
source = fb.SignalSource(data = input_string)
coder = fb.SignalCoder(pb = source, table = coding_table)
decoder = fb.SignalDecoder(pb = coder, table = coding_table)
reciever = fb.SignalReciever(pb = decoder)

block_chain = (source, coder, decoder, reciever)

#Sending the data through the chain
for block in block_chain:
    if not block.next_block is None:
        block.send_data()

