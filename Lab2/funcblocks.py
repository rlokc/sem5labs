import random
from constants import *

'''
All of the "building block" classes
'''


'''
Parent class of function blocks, that transmit data to each other
It's kind of like a linked list, but I really want to make it all abstract and stuff, so here it is.
'''


class FunctionBlock:
    
    previous_block = None
    next_block = None
    data = ""

    def __init__(self, pb = None, nb = None, data = None):
        self.previous_block = pb
        self.next_block = nb
        self.data = data
        #Setting previous/next nodes of next/previous nodes of this node to self
        if not pb is None:
            pb.set_next_block(self)
        if not nb is None:
            nb.set_previous_block(self)
        

    def send_data(self):
        #TODO: Print actual object names instead of their classes
        print(self.__class__.__name__, 'sent "', self.data, '" to', self.next_block.__class__.__name__)
        #TODO: Implement channel errors
        self.next_block.recieve_data(self.data)
    
    def recieve_data(self, data):
        self.data = data        
        print(self.__class__.__name__, 'recieved "', self.data, '" from', self.previous_block.__class__.__name__)

    def set_next_block(self, nb):
        self.next_block = nb

    def set_previous_block(self, pb):
        self.previous_block = pb


class SignalSource(FunctionBlock):
    
    def recieve_data(self, data):
        print("ERROR: Sending data to source")


class SignalCoder(FunctionBlock):
    
    coding_table = {}
    zeroes = 0
    ones = 0

    error_prob = 0

    def __init__(self, pb = None, nb = None, data = None, table = None):
        super(SignalCoder, self).__init__(pb, nb, data)
        self.coding_table = table
 
    #Splitting the data into block_length chunks for decoding
    def split_into_chunks(self, data, block_length):
        new_data = []
        while len(data) >= block_length:
            buf = data[:block_length]
            data = data[block_length:]
            new_data.append(buf)
        return new_data

    def code_data(self, coding_table = None):
        table = self.coding_table
        if not coding_table is None:
            table = coding_table
        #Replacing the old data with the coded data
        self.data = self.split_into_chunks(self.data, BLOCK_LENGTH) 
        self.data = ''.join([table[ch] for ch in self.data])
        #There must be a better way to do this
        for ch in self.data:
            if ch == '0':
                self.zeroes += 1
            else:
                self.ones += 1

    def send_data(self):
        #Before we send the data, we gotta code it!
        self.code_data()
        print(self.__class__.__name__, 'sent "', self.data, '" to', self.next_block.__class__.__name__)
        self.next_block.recieve_data(self.data)


'''
Inherits from coder, because both do the same thing, but in reversal
'''
class SignalDecoder(SignalCoder):
    def __init__(self, pb = None, nb = None, data = None, table = None):
        if not table is None:
            #Reversing the coding table, so that decoding is easy
            table = {value: key for key, value in table.items()}
        super(SignalDecoder, self).__init__(pb, nb, data, table)

    def code_data(self, coding_table = None):
        table = self.coding_table
        if not coding_table is None:
            table = coding_table
        data_buffer = ''
        code_buffer = ''
        for ch in self.data:
            code_buffer += ch
            if code_buffer in table:
                data_buffer += table[code_buffer]
                code_buffer = ''
        self.data = data_buffer        

class SignalChannel(FunctionBlock):
 
    def __init__(self, pb = None, nb = None, data = None,  error_prob = 0):
        super(SignalChannel, self).__init__(pb, nb, data)
        self.error_prob = error_prob
    
    def add_errors(self):
        new_data = ''
        for ch in self.data:
            if random.random() <= self.error_prob:
                ch = '1' if ch == '0' else '0'
            new_data += ch
        self.data = new_data

    def recieve_data(self, data):
        super(SignalChannel, self).recieve_data(data)
        self.add_errors()
        print(self.__class__.__name__, 'has added errors, now the data is', self.data)


class SignalReciever(FunctionBlock):

    def send_data(self, data):
        print("ERROR: Sending data from reciever")

'''
Now for error-proof coding!
'''

class SignalEPCoder(FunctionBlock):

    gen_matrice = [[],
                   [],
                   [],
                   [],]

    chk_matrice = [[],
                   [],
                   [],
                   [],
                   [],
                   [],
                   [],]

    def recieve_data(self, data):
        super(SignalEPCoder, self).recieve_data(data)
        self.err_proof_code()
    
    def err_proof_code(self):
        pass


