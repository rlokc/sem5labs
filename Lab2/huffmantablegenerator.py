'''
Coding table generator for Huffman algorithm
'''

DEBUG = False
coding_table = {}

def generate_table(source_string, block_length = 1, equal_probs = False):

    global coding_table
    probability_dict = generate_prob_table(source_string, block_length, equal_probs)
    #Commence algorithm!
    while len(probability_dict) > 1:    
        #Step1: Sort the dictionary by probabilites, and get the two least used values
        least_used_values = sorted(probability_dict.keys(), key=probability_dict.get)[:2]
        if DEBUG:
            print(least_used_values)
        sum_prob = 0
        #Step2: Add 0 or 1 to the left of the coding of each letter in these strings
        for index, word in enumerate(least_used_values):
            buf = ''
            for ch in word:
                buf = ''.join([buf, ch])
                if len(buf) == block_length:
                    coding_table[buf] = ''.join([str(index), coding_table[buf]])
                    buf = ''
            #Removing the old word from the dictionary and adding its probability to the new one
            sum_prob += probability_dict.pop(word)
        if DEBUG:
            print(coding_table)
        #Step3: Create a new word, consisting of used 2, and put it into the dictionary
        probability_dict[''.join(least_used_values)] = sum_prob
        #Step4: Rince and repeat, until we have only 1 word in the dictionary

    return coding_table

def generate_prob_table(source_string, block_length = 1, equal_probs = False):
    global coding_table
    probability_dict = {}

    if equal_probs:
        #repetition makes me sad
        for character in source_string:
            if character not in probability_dict:
                probability_dict[character] = 1
                coding_table[character] = ""
        for character in probability_dict:
            probability_dict[character] = 1.0/len(probability_dict)
        if block_length > 1:
            #Resetting the coding table
            coding_table = {}
            new_prob_dict = {}
            for char1 in probability_dict:
                for char2 in probability_dict:
                    word = ''.join([char1, char2])
                    new_prob_dict[word] = probability_dict[char1] * probability_dict[char2]
                    coding_table[word] = ''
            probability_dict = new_prob_dict
        return probability_dict

    for character in source_string:
        if character not in probability_dict:
            probability_dict[character] = 1
            #Adding our character to the final coding table
            coding_table[character] = ""
        else:
            probability_dict[character] += 1
    #Counting probabilities of each character
    for ch in probability_dict:
        probability_dict[ch] = probability_dict[ch] / float(len(source_string))
    #Now implementing block coding. For now, only for the 2 length
    #TODO: If needed, find a way to implement variable block length
    if block_length > 1:
        #Resetting the coding table
        coding_table = {}
        new_prob_dict = {}
        for char1 in probability_dict:
            for char2 in probability_dict:
                word = ''.join([char1, char2])
                new_prob_dict[word] = probability_dict[char1] * probability_dict[char2]
                coding_table[word] = ''
        probability_dict = new_prob_dict
    
    if DEBUG:
        print(probability_dict)
    return probability_dict
                
if __name__ == "__main__":
    DEBUG = True
    generate_table("aaaabbbccd", 2)
