import sys, funcblocks as fb, huffmantablegenerator as tablegen
from constants import *

HELPSTRING = '''Signal... something module, which uses Huffman algorithm for coding and decoding data\nUsage: either pass the string as an argument, or pass nothing and input it yourself'''

equal_probs = False

if len(sys.argv) == 1:
    input_string = input("Input the string:\n")
    if input_string.replace(' ','') == '':
        input_string = "Allan please add test string"
elif sys.argv[1] in ['-h', '--h', '/?']:
    print(HELPSTRING)
    sys.exit(0)
elif sys.argv[1] == '--equal':
    equal_probs = True
    input_string = ' '.join(sys.argv[2:])
else:
    #TODO: more than one input, maybe?
    input_string = ' '.join(sys.argv[1:])

#Adding additional whitespace to the end, if the string isn't divisible by block length
lacking_sym_n = len(input_string) % BLOCK_LENGTH
for i in range(lacking_sym_n):
    input_string += input_string + ' '

prob_table = tablegen.generate_prob_table(input_string, BLOCK_LENGTH, equal_probs)
coding_table = tablegen.generate_table(input_string, BLOCK_LENGTH, equal_probs)
print('Huffman coding table:\n', coding_table)
source = fb.SignalSource(data = input_string)
coder = fb.SignalCoder(pb = source, table = coding_table)
channel = fb.SignalChannel(pb = coder, error_prob = ERROR_PROBABILITY)
decoder = fb.SignalDecoder(pb = channel, table = coding_table)
reciever = fb.SignalReciever(pb = decoder)

block_chain = (source, coder, channel, decoder, reciever)

#Sending the data through the chain
for block in block_chain:
    if not block.next_block is None:
        block.send_data()

print("Total binary numbers sent:", len(coder.data))
print("Of them:")
print("0s:", coder.zeroes)
print("1s:", coder.ones)
avg_bin_per_symbol = 0
for key in coding_table.keys():
    avg_bin_per_symbol += len(coding_table[key]) * prob_table[key]
print("Average ammount of binary numbers per symbol:", avg_bin_per_symbol)
print(prob_table)
